package com.jobportalsql.repoimpl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.jobportalsql.entities.UserEntity;
import com.jobportalsql.ilistdto.IUserListDto;
import com.jobportalsql.page.PaginationUtil;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

//@Repository
//@Transactional
public class UserRepositoryImpl {
//implements UserRepository {

	@PersistenceContext
	private EntityManager entityManager;

	public UserEntity findByEmailIgnoreCase(String email) {
		UserEntity userEntity = (UserEntity) entityManager
				.createNativeQuery("SELECT * FROM users u WHERE u.email ILIKE '%:user_email%' and u.is_active=true")
				.setParameter("user_email", email).getSingleResult();
		return userEntity;
	}

	public Boolean existsByEmail(String email) {
		boolean flag = (boolean) entityManager.createNativeQuery(
				"SELECT EXISTS(SELECT 1 FROM users u WHERE u.email ilike '%BALAJI@NIMAPINFOTECH.COM%' and u.is_active=true)")
				.setParameter("user_email", email).getSingleResult();
		return flag;
	}

	public Page<IUserListDto> findById(Long id, Pageable pageable, Class<IUserListDto> class1) {

		String query = "SELECT u.id as Id, u.name as Name, u.email as Email FROM users u WHERE u.id=:id";

		return PaginationUtil.getPage(entityManager, query, pageable, class1, id);
	}

	public Page<IUserListDto> findByOrderByIdDesc(Pageable pageable, Class<IUserListDto> class1) {
		// TODO Auto-generated method stub
		return null;
	}

}
