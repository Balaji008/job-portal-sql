package com.jobportalsql.repoimpl;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.jobportalsql.entities.UserRoleEntity;
import com.jobportalsql.ilistdto.IUserRoleListDto;
import com.jobportalsql.page.PaginationUtil;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.servlet.http.HttpServletResponse;

//@Repository
//@Transactional
public class UserRoleRepositoryImpl {
//implements UserRoleRepository {

	@PersistenceContext
	private EntityManager entityManager;

	public void saveUserRole(UserRoleEntity userRole) {
		entityManager
				.createNativeQuery(
						"INSERT INTO user_roles (user_id, role_id, is_active, created_at, updated_at, created_by) "
								+ "VALUES (:userId, :roleId, :isActive, :createdAt, :updatedAt, :createdBy)")
				.setParameter("userId", userRole.getUser().getId()).setParameter("roleId", userRole.getRole().getId())
				.setParameter("isActive", true).setParameter("createdAt", userRole.getCreatedAt())
				.setParameter("updatedAt", userRole.getUpdatedAt()).setParameter("createdBy", userRole.getCreatedBy())
				.executeUpdate();
	}

	public void deleteUserRole(UserRoleEntity userRole) {
		entityManager.createNativeQuery("UPDADE user_roles SET is_active=false where id=:userRoleId")
				.setParameter("userRoleId", userRole.getId()).executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public List<UserRoleEntity> findByRoleId(Long roleId) {
		return entityManager.createNativeQuery("select * from user_roles where role_id=:roleId where is_active=true",
				UserRoleEntity.class).setParameter("roleId", roleId).getResultList();
	}

	public UserRoleEntity findByUserId(Long userId) {
		return (UserRoleEntity) entityManager
				.createNativeQuery("select * from user_roles where user_id=:userId where is_active=true",
						UserRoleEntity.class)
				.setParameter("userId", userId).getSingleResult();
	}

	public boolean userExists(Long userId) {
		Object result = entityManager.createNativeQuery(
				"SELECT EXISTS(SELECT 1 FROM user_roles ur WHERE ur.user_id=:userId and ur.is_active=true LIMIT 1)")
				.setParameter("userId", userId).getSingleResult();
		return ((Number) result).intValue() == 1;
	}

	public Page<IUserRoleListDto> getAllUsersByRole(Long roleId, Pageable pageable, Class<IUserRoleListDto> class1) {
		String query = "SELECT u.id as Id, u.email as Email,u.name as Name,u.gender as Gender,u.phone_number as PhoneNumber,r.role_name as RoleName "
				+ "FROM users u JOIN user_roles ur ON u.id=ur.user_id JOIN roles r ON r.id=ur.role_id WHERE r.id=:roleId and is_active=trueORDER BY u.id DESC";

		return PaginationUtil.getPage(entityManager, query, pageable, class1, roleId);
	}

	public Page<IUserRoleListDto> getAllUsersBySearch(String search, Pageable pageable,
			Class<IUserRoleListDto> class1) {
		String query = "SELECT u.id as Id, u.email as Email, u.name as Name, u.gender as Gender, u.phone_number as PhoneNumber, r.role_name as RoleName "
				+ "FROM users u JOIN user_roles ur ON u.id=ur.user_id JOIN roles r ON r.id=ur.role_id "
				+ "WHERE LOWER(u.email) LIKE LOWER(CONCAT('%', :search, '%')) "
				+ "OR LOWER(u.name) LIKE LOWER(CONCAT('%', :search, '%')) "
				+ "OR LOWER(u.gender) LIKE LOWER(CONCAT('%', :search, '%')) "
				+ "OR LOWER(u.phone_number) LIKE LOWER(CONCAT('%', :search, '%')) "
				+ "OR LOWER(r.role_name) LIKE LOWER(CONCAT('%', :search, '%')) and u.is_active=true and r.is_active=true and ur.is_active=true ORDER BY u.id Desc";
		@SuppressWarnings("unchecked")
		List<IUserRoleListDto> userList = entityManager.createNativeQuery(query, class1).setParameter("search", search)
				.setFirstResult((int) pageable.getOffset()).setMaxResults(pageable.getPageSize()).getResultList();
		String countQuery = "SELECT COUNT(*) FROM users u JOIN user_roles ur ON u.id=ur.user_id JOIN roles r ON r.id=ur.role_id "
				+ "WHERE LOWER(u.email) LIKE LOWER(CONCAT('%', :search, '%')) "
				+ "OR LOWER(u.name) LIKE LOWER(CONCAT('%', :search, '%')) "
				+ "OR LOWER(u.gender) LIKE LOWER(CONCAT('%', :search, '%')) "
				+ "OR LOWER(u.phone_number) LIKE LOWER(CONCAT('%', :search, '%')) "
				+ "OR LOWER(r.role_name) LIKE LOWER(CONCAT('%', :search, '%')) and u.is_active=true and r.is_active=true and ur.is_active=true";
		Long count = (Long) entityManager.createNativeQuery(countQuery).setParameter("search", search)
				.getSingleResult();
		Page<IUserRoleListDto> page = new PageImpl<>(userList, pageable, count);
		return page;
	}

	public List<IUserRoleListDto> getAllUsers(HttpServletResponse response, Class<IUserRoleListDto> class1) {
		String query = "SELECT u.id as Id, u.email as Email, u.name as Name, u.gender as Gender, u.phone_number as PhoneNumber, r.role_name as RoleName "
				+ "FROM users u JOIN user_roles ur ON u.id=ur.user_id JOIN roles r ON r.id=ur.role_id "
				+ "WHERE r.role_name='User' and u.is_active=true and r.is_active=true and ur.is_active=true ORDER BY u.id DESC";
		@SuppressWarnings("unchecked")
		List<IUserRoleListDto> userList = entityManager.createNativeQuery(query, class1).getResultList();
		response.setHeader("Content-Disposition", "attachment; filename=users.csv");
		return userList;
	}

	public List<IUserRoleListDto> getAllRecruiters(HttpServletResponse response, Class<IUserRoleListDto> class1) {
		String query = "SELECT u.id as Id, u.email as Email, u.name as Name, u.gender as Gender, u.phone_number as PhoneNumber, r.role_name as RoleName "
				+ "FROM users u JOIN user_roles ur ON u.id=ur.user_id JOIN roles r ON r.id=ur.role_id "
				+ "WHERE r.role_name='Recruiter' and u.is_active=true and r.is_active=true and ur.is_active=true ORDER BY u.id DESC";
		@SuppressWarnings("unchecked")
		List<IUserRoleListDto> recruiterList = entityManager.createNativeQuery(query, class1).getResultList();
		response.setHeader("Content-Disposition", "attachment; filename=recruiters.csv");
		return recruiterList;
	}

}