package com.jobportalsql.dto;

import java.util.List;

import com.jobportalsql.entities.JobDetails;
import com.jobportalsql.entities.RecruiterDetails;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecruiterJobResponseDto {

	private RecruiterDetails recruiterDetails;

	private List<JobDetails> jobDetails;
}
