package com.jobportalsql.dto;

import java.util.List;

import com.jobportalsql.entities.JobDetails;
import com.jobportalsql.entities.UserDetails;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserJobResponseDto {

	private UserDetails userDetails;

	private List<JobDetails> jobDetails;
}
