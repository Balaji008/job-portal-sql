package com.jobportalsql.service;

import org.springframework.data.domain.Page;

import com.jobportalsql.dto.JobApplyDto;
import com.jobportalsql.dto.JobDto;
import com.jobportalsql.dto.JobUserResponseDto;
import com.jobportalsql.dto.UserJobResponseDto;

public interface UserJobService {

	JobDto applyForJob(Long userId, JobApplyDto jobApplyDto) throws Exception;

	void cancelJobApplication(Long userId, Long jobId);

	Page<UserJobResponseDto> getAllJobsByUser(Long userId, String pageNumber, String pageSize);

	Page<JobUserResponseDto> getAllUsersByJob(Long jobId, String pageNumber, String pageSize);

//	Page<IUserJobsListDto> getAllUserJob(String pageNumber, String pageSize);

	Page<JobUserResponseDto> getAllUserJob(String pageNumber, String pageSize);

	Page<JobUserResponseDto> getAddedJobs(Long userId, String pageNumber, String pageSize);
}
