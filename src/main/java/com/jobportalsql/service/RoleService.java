package com.jobportalsql.service;

import com.jobportalsql.dto.RoleDto;

public interface RoleService {

	RoleDto addRole(RoleDto roleDto);

	RoleDto updateRole(RoleDto roleDto);

	void deleteRole(Long id);

}
