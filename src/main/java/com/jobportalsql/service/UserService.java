package com.jobportalsql.service;

import org.springframework.data.domain.Page;

import com.jobportalsql.dto.UserDto;
import com.jobportalsql.dto.UserResponseDto;
import com.jobportalsql.ilistdto.IUserListDto;
import com.jobportalsql.ilistdto.IUserRoleListDto;

public interface UserService {

	Page<IUserRoleListDto> getAllUsersBySerach(String search, String pageNumber, String pageSize);

	Page<IUserRoleListDto> getAllUsers(String pageNumber, String pageSize);

	Page<IUserRoleListDto> getAllRecruiters(String pageNumber, String pageSize);

	Page<IUserRoleListDto> getAllAdmin(String pageNumber, String pageSize);

	Page<IUserListDto> getUserById(Long id, String pageNumber, String pageSize);

//	Page<IUserListDto> getUserByName(String name, String pageNumber, String pageSize);

	void deleteUser(Long id);

	UserResponseDto updateUser(Long id, UserDto userDto, Long adminId);

}
