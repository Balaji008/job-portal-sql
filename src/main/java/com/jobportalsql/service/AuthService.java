package com.jobportalsql.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.jobportalsql.dto.AuthDto;
import com.jobportalsql.dto.UserResponseDto;
import com.jobportalsql.entities.UserEntity;
import com.jobportalsql.util.JwtAuthenticationRequest;
import com.jobportalsql.util.JwtAuthenticationResponse;

public interface AuthService extends UserDetailsService {

	JwtAuthenticationResponse login(JwtAuthenticationRequest request);

	String refreshAndGetAuthenticationToken(String refreshToken) throws Exception;

	List<String> permissions(Long userId);

	UserEntity userInformation(String email);

	JwtAuthenticationResponse loginWithOtp(String email);

	UserResponseDto registerUser(AuthDto AuthDto);

	UserResponseDto registerRecruiter(AuthDto authDto, Long adminId);

//	boolean confirmPassword(ForgotPasswordDto forgotPasswordDto);

}
