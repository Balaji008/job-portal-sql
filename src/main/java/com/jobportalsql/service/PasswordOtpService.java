package com.jobportalsql.service;

import com.jobportalsql.dto.EmailOtpDto;
import com.jobportalsql.dto.ForgotPasswordDto;

import jakarta.mail.MessagingException;

public interface PasswordOtpService {

	String passwordResetOtp(EmailOtpDto emailOtpDto) throws MessagingException;

	boolean validateOtp(ForgotPasswordDto forgotPasswordDto);

}
