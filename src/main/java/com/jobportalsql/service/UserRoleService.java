package com.jobportalsql.service;

import java.util.List;

import com.jobportalsql.dto.UserRoleDto;
import com.jobportalsql.ilistdto.IUserRoleListDto;

public interface UserRoleService {

	List<IUserRoleListDto> assignRoleToUser(UserRoleDto userRoleDto);

	List<IUserRoleListDto> updateUserRole(UserRoleDto userRoleDto, Long id);

	void deleteUserRole(Long id);

	List<IUserRoleListDto> getAllUserRoles();
}
