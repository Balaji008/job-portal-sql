package com.jobportalsql.service;

import com.jobportalsql.dto.EmailOtpDto;
import com.jobportalsql.dto.OtpDto;

import jakarta.mail.MessagingException;

public interface OtpService {

	String sendOtp(OtpDto otpDto) throws MessagingException;

	boolean validateOtp(String email, String otp);

	String passwordResetOtp(EmailOtpDto emailOtpDto) throws MessagingException;
}
