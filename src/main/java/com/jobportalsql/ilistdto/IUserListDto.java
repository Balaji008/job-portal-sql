package com.jobportalsql.ilistdto;

public interface IUserListDto {

	Long getId();

	String getName();

	String getEmail();

}
