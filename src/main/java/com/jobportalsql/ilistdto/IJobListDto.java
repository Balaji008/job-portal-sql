package com.jobportalsql.ilistdto;

public interface IJobListDto {

	Long getId();

	String getTitle();

	String getDescription();

}
