package com.jobportalsql.ilistdto;

public interface IPermissionListDto {

	Long getId();

	String getPermissionName();

}
