package com.jobportalsql.ilistdto;

import java.util.List;

public interface IRolePermissionListDto {

	String getRoleName();

	List<String> getPermissionName();
}
