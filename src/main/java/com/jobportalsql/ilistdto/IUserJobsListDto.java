package com.jobportalsql.ilistdto;

public interface IUserJobsListDto {

	Long getId();

	String getName();

	String getEmail();

	Long getJobId();

	String getJobTitle();
}
