package com.jobportalsql.ilistdto;

public interface IUserRoleListDto {

	Long getId();

	String getEmail();

	String getName();

	String getGender();

	String getPhoneNumber();

	String getRoleName();

}
