package com.jobportalsql.page;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.Selection;

public class PaginationUtil {

	public static <T> Page<T> getPage(EntityManager entityManager, String query, Pageable pageable, Class<T> clazz,
			Object... params) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = builder.createQuery(clazz);
		Root<?> root = criteriaQuery.from(clazz);

		TypedQuery<T> typedQuery = entityManager.createQuery(query, clazz);
		typedQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
		typedQuery.setMaxResults(pageable.getPageSize());

		for (int i = 0; i < params.length; i++) {
			typedQuery.setParameter(i + 1, params[i]);
		}

		Page<T> page = new PageImpl<>(typedQuery.getResultList(), pageable,
				getTotalCount(entityManager, criteriaQuery, root, params));

		return page;
	}

	@SuppressWarnings("unchecked")
	private static <T> long getTotalCount(EntityManager entityManager, CriteriaQuery<T> criteriaQuery, Root<?> root,
			Object... params) {
		criteriaQuery.select((Selection<? extends T>) entityManager.getCriteriaBuilder().count(root));
		TypedQuery<Long> query = (TypedQuery<Long>) entityManager.createQuery(criteriaQuery);
		for (int i = 0; i < params.length; i++) {
			query.setParameter(i + 1, params[i]);
		}
		return query.getSingleResult();
	}

}
