package com.jobportalsql.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/gen")
public class GeneralController {

	@GetMapping("/start")
	public String start() {
		return "Success";
	}
}
