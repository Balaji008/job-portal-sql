package com.jobportalsql.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jobportalsql.entities.FileUploadEntity;

public interface FileUploadRepository extends JpaRepository<FileUploadEntity, Long> {

}
