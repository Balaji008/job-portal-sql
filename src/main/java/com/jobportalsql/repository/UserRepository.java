package com.jobportalsql.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.jobportalsql.entities.UserEntity;
import com.jobportalsql.ilistdto.IUserListDto;

import io.lettuce.core.dynamic.annotation.Param;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

//	Page<IUserListDto> findByNameContainingIgnoreCase(String name, Pageable pageable, Class<IUserListDto> class1);

	@Query(value = "SELECT * FROM users u WHERE u.email ILIKE '%:email%' and u.is_active=true", nativeQuery = true)
	UserEntity findByEmailIgnoreCase(@Param("email") String email);

	@Query(value = "SELECT EXISTS(SELECT 1 FROM users u WHERE u.email ilike '%:email%' and u.is_active=true)", nativeQuery = true)
	Boolean existsByEmail(@Param("email") String email);

	@Query(value = "SELECT u.id as Id, u.name as Name, u.email as Email FROM users u WHERE u.id=:id", nativeQuery = true)
	Page<IUserListDto> findById(@Param("id") Long id, Pageable pageable, Class<IUserListDto> class1);

	@Query(value = "SELECT u.id as Id,u.name as name,u.email as Email from users u order by u.id desc", nativeQuery = true)
	Page<IUserListDto> findByOrderByIdDesc(Pageable pageable, Class<IUserListDto> class1);

}
