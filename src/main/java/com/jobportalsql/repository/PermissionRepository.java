package com.jobportalsql.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.jobportalsql.entities.PermissionEntity;
import com.jobportalsql.ilistdto.IPermissionListDto;

public interface PermissionRepository extends JpaRepository<PermissionEntity, Long> {

	@Query(value = "SELECT p.id as Id, p.permission_name as PermissionName from permissions p order by p.id desc", nativeQuery = true)
	List<IPermissionListDto> findByOrderByIdDesc(Class<IPermissionListDto> class1);

//	List<IPermissionListDto> findByOrderByIdDesc(Class<IPermissionListDto> class1);

}
