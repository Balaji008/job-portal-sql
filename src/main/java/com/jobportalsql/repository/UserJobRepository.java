package com.jobportalsql.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jobportalsql.entities.UserEntity;
import com.jobportalsql.entities.UserJobEntity;
import com.jobportalsql.ilistdto.ForNesting;
import com.jobportalsql.ilistdto.IUserJobsListDto;

import jakarta.servlet.http.HttpServletResponse;

public interface UserJobRepository extends JpaRepository<UserJobEntity, Long> {

	@Query(value = "SELECT * from user_jobs where job_id=:jobId and user_id=:userId", nativeQuery = true)
	UserJobEntity findByJobAndUser(@Param("jobId") Long job, @Param("userId") Long user);

	@Query(value = "SELECT EXISTS(SELECT * from user_jobs where job_id=:jobId and user_id=:userId)", nativeQuery = true)
	boolean existsByJobAndUser(@Param("jobId") Long job, @Param("userId") Long user);

	@Query(value = "select u.id as Id, u.name as Name , u.email as Email, j.id as JobId, j.title as JobTitle from user_jobs uj\r\n"
			+ "join users u on uj.user_id=u.id \r\n"
			+ "join jobs j on uj.job_id=j.id where uj.user_id=:userId order by uj.id desc", nativeQuery = true)
	Page<IUserJobsListDto> getAllJobsByUser(@Param("userId") Long userId, Pageable pageable,
			Class<IUserJobsListDto> class1);

	@Query(value = "select u.id as Id, u.name as Name , u.email as Email, j.id as JobId, j.title as JobTitle from user_jobs uj\r\n"
			+ "join users u on uj.user_id=u.id \r\n"
			+ "join jobs j on uj.job_id=j.id where uj.job_id=:jobId order by uj.id desc", nativeQuery = true)
	Page<IUserJobsListDto> getAllUsersByJob(@Param("jobId") Long jobId, Pageable pageable,
			Class<IUserJobsListDto> class1);

	@Query(value = "select u.id as Id, u.name as Name , u.email as Email, j.id as JobId, j.title as JobTitle from user_jobs uj\r\n"
			+ "			join users u on uj.user_id=u.id \r\n"
			+ "			join jobs j on uj.job_id=j.id where j.created_by=:userId order by uj.id desc", nativeQuery = true)
	Page<IUserJobsListDto> getAllCreatedJobs(Long userId, Pageable pageable, Class<IUserJobsListDto> class1);

	@Query(value = "select u.id as Id, u.name as Name , u.email as Email, j.id as JobId, j.title as JobTitle from user_jobs uj\r\n"
			+ "join users u on uj.user_id=u.id \r\n"
			+ "join jobs j on uj.job_id=j.id order by uj.id desc", nativeQuery = true)
	Page<IUserJobsListDto> getAllUserJobs(Pageable pageable, Class<IUserJobsListDto> class1);

	@Query(value = "select u.id as Id, u.name as Name , u.email as Email, j.id as JobId, j.title as JobTitle from user_jobs uj\r\n"
			+ "join users u on uj.user_id=u.id \r\n"
			+ "join jobs j on uj.job_id=j.id order by uj.id desc", nativeQuery = true)
	List<IUserJobsListDto> getAllUserJobsList(HttpServletResponse response, Class<IUserJobsListDto> class1);

	List<ForNesting> findAllJobByUser(UserEntity user, Class<ForNesting> class1);
}
