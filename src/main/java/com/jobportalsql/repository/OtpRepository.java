package com.jobportalsql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jobportalsql.entities.OtpEntity;

public interface OtpRepository extends JpaRepository<OtpEntity, Long> {

	@Query(value = "SELECT * from login_otp l where l.email ILIKE ':email'", nativeQuery = true)
	OtpEntity findByEmailIgnoreCase(@Param("email") String email);
}
