package com.jobportalsql.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jobportalsql.entities.JobEntity;
import com.jobportalsql.ilistdto.IJobListDto;
import com.jobportalsql.ilistdto.IUserJobsListDto;

public interface JobRepository extends JpaRepository<JobEntity, Long> {

	@Query(value = "SELECT j.id as Id, j.title as Title,j.description as Description from jobs j where created_by=:createdBy order by j.id desc", nativeQuery = true)
	Page<IJobListDto> findByCreatedBy(@Param("createdBy") Long createdBy, Pageable pageable, Class<IJobListDto> class1);

	@Query(value = "SELECT j.id as Id, j.title as Title,j.description as Description from jobs j order by j.id desc", nativeQuery = true)
	Page<IJobListDto> findByOrderByIdDesc(Pageable pageable, Class<IJobListDto> class1);

	@Query(value = "SELECT j.id as Id, j.title as Title, j.description as Description FROM jobs j WHERE LOWER(j.title) LIKE LOWER('%' || :title || '%')\r\n"
			+ "OR LOWER(j.description) LIKE LOWER('%' || :description || '%')\r\n"
			+ "ORDER BY id DESC", nativeQuery = true)
	Page<IJobListDto> findByTitleContainingIgnoreCaseOrDescriptionContainingIgnoreCaseOrderByIdDesc(
			@Param("title") String title, @Param("description") String description, Pageable pageable,
			Class<IJobListDto> class1);

	@Query(value = "SELECT EXISTS(SELECT * from jobs where id=:jobId and created_by=:createdBy)", nativeQuery = true)
	boolean existsByIdAndCreatedBy(@Param("jobId") Long jobId, @Param("createdBy") Long createdBy);

	@Query(value = "SELECT u.id as Id, u.name as Name,u.email as Email, j.id as JobId, j.title as JobTitle from users u join jobs j on u.id=j.created_by\r\n"
			+ "and u.is_active=true and j.is_active=true", nativeQuery = true)
	Page<IUserJobsListDto> getAllJobs(Pageable pageable, Class<IUserJobsListDto> class1);
}
