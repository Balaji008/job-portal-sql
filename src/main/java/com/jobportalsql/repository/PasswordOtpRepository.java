package com.jobportalsql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jobportalsql.entities.PasswordOtpEntity;

public interface PasswordOtpRepository extends JpaRepository<PasswordOtpEntity, Long> {

	@Query(value = "SELECT * from password_otp p where p.email ILIKE ':email'", nativeQuery = true)
	PasswordOtpEntity findByEmailIgnoreCase(@Param("email") String email);

}
