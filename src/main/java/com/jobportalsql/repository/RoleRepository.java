package com.jobportalsql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jobportalsql.entities.RoleEntity;

public interface RoleRepository extends JpaRepository<RoleEntity, Long> {

	@Query(value = "SELECT * FROM roles WHERE role_name ILIKE %:roleName%", nativeQuery = true)
	RoleEntity findByRoleNameIgnoreCase(@Param("roleName") String roleName);

//	RoleEntity findByRoleName(String name);
}
