package com.jobportalsql.serviceimpl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jobportalsql.constants.Constants;
import com.jobportalsql.dto.EmailOtpDto;
import com.jobportalsql.dto.OtpDto;
import com.jobportalsql.entities.OtpEntity;
import com.jobportalsql.entities.UserEntity;
import com.jobportalsql.exception.ResourceNotFoundException;
import com.jobportalsql.repository.OtpRepository;
import com.jobportalsql.service.AuthService;
import com.jobportalsql.service.EmailService;
import com.jobportalsql.service.OtpService;
import com.jobportalsql.util.ErrorKeyConstants;
import com.jobportalsql.util.ErrorMessageConstants;
import com.jobportalsql.util.OtpUtil;

import jakarta.mail.MessagingException;

@Service
public class OtpServiceImpl implements OtpService {

	@Autowired
	private EmailService emailService;

	@Autowired
	private OtpRepository otpRepository;

	@Autowired
	private AuthService authService;

	@Override
	public String sendOtp(OtpDto otpDto) throws MessagingException {

		OtpEntity otp = new OtpEntity();
		otp = this.otpRepository.findByEmailIgnoreCase(otpDto.getEmail().trim());

		if (otp != null) {
			String otp1 = new String(OtpUtil.otp());
			otp.setOtp(otp1);
			otp.setGeneratedAt(LocalDateTime.now());
			otpRepository.save(otp);
			emailService.sendEmail(otpDto.getEmail(), Constants.LOGIN_OTP, OtpUtil.message(otp1));

			return otp1;

		} else {
			UserEntity user = null;
			OtpEntity otpEntity = new OtpEntity();
			user = this.authService.userInformation(otpDto.getEmail().trim());
			if (user != null) {
				otpEntity.setEmail(otpDto.getEmail());
				String otp1 = new String(OtpUtil.otp());
				otpEntity.setOtp(otp1);
				otpEntity.setGeneratedAt(LocalDateTime.now());
				otpRepository.save(otpEntity);
				emailService.sendEmail(otpDto.getEmail(), Constants.LOGIN_OTP, OtpUtil.message(otp1));
				return otp1;
			}

		}

		return null;
	}

	@Override
	public String passwordResetOtp(EmailOtpDto emailOtpDto) throws MessagingException {

		OtpEntity otp = new OtpEntity();
		otp = this.otpRepository.findByEmailIgnoreCase(emailOtpDto.getEmail().trim());

		if (otp != null) {
			String otp1 = new String(OtpUtil.otp());
			otp.setOtp(otp1);
			otp.setGeneratedAt(LocalDateTime.now());
			otpRepository.save(otp);
			emailService.sendEmail(emailOtpDto.getEmail(), Constants.LOGIN_OTP, OtpUtil.resetPasswordMessage(otp1));

			return otp1;

		} else {
			UserEntity user = null;
			OtpEntity otpEntity = new OtpEntity();
			user = this.authService.userInformation(emailOtpDto.getEmail().trim());
			if (user != null) {
				otpEntity.setEmail(emailOtpDto.getEmail());
				String otp1 = new String(OtpUtil.otp());
				otpEntity.setOtp(otp1);
				otpEntity.setGeneratedAt(LocalDateTime.now());
				otpRepository.save(otpEntity);
				emailService.sendEmail(emailOtpDto.getEmail(), Constants.LOGIN_OTP, OtpUtil.message(otp1));
				return otp1;
			}

		}

		return null;
	}

	@Override
	public boolean validateOtp(String email, String otp) {

		OtpEntity otpEntity = this.otpRepository.findByEmailIgnoreCase(email);

		if (otpEntity == null) {
			throw new ResourceNotFoundException(ErrorKeyConstants.OTP_E032201, ErrorMessageConstants.OTP_NOT_FOUND);
		}
		LocalDateTime currentTime = LocalDateTime.now();
		LocalDateTime otpExpirationTime = otpEntity.getGeneratedAt().plusMinutes(15); // OTP expiration time
		if (currentTime.isAfter(otpExpirationTime)) {
			// OTP has expired
			otpEntity.setOtp(""); // Clear the OTP
			this.otpRepository.save(otpEntity); // Delete the OTP entity from the database
			return false;
		}

		else if (otp.equalsIgnoreCase(otpEntity.getOtp())) {
			otpEntity.setOtp("");
			this.otpRepository.save(otpEntity);
			return true;

		}
		return false;
	}
}
