package com.jobportalsql.serviceimpl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.jobportalsql.constants.Constants;
import com.jobportalsql.dto.JobDto;
import com.jobportalsql.dto.RecruiterJobResponseDto;
import com.jobportalsql.entities.JobDetails;
import com.jobportalsql.entities.JobEntity;
import com.jobportalsql.entities.RecruiterDetails;
import com.jobportalsql.exception.ResourceNotFoundException;
import com.jobportalsql.ilistdto.IJobListDto;
import com.jobportalsql.ilistdto.IUserJobsListDto;
import com.jobportalsql.page.Pegination;
import com.jobportalsql.repository.JobRepository;
import com.jobportalsql.service.JobService;
import com.jobportalsql.util.ErrorKeyConstants;
import com.jobportalsql.util.ErrorMessageConstants;

@Service
public class JobServiceImpl implements JobService {

	@Autowired
	private JobRepository jobRepository;

	@Override
	public JobDto addJob(Long userId, JobDto jobDto) {
		JobEntity jobEntity = new JobEntity();
		jobEntity.setTitle(jobDto.getTitle());
		jobEntity.setCreatedBy(userId);
		jobEntity.setDescription(jobDto.getDescription());
		this.jobRepository.save(jobEntity);
		jobDto.setId(jobEntity.getId());

		return jobDto;
	}

	@Override
	public JobDto updateJob(Long userId, JobDto jobDto, Long jobId) {
		JobEntity jobEntity = this.jobRepository.findById(jobId)
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.JOB_E031401,
						ErrorMessageConstants.JOB_NOT_FOUND));

		jobEntity.setTitle(jobDto.getTitle());
		jobEntity.setDescription(jobDto.getDescription());
		this.jobRepository.save(jobEntity);
		jobDto.setId(jobEntity.getId());
		return jobDto;

	}

	@Override
	public Page<IJobListDto> getAddedJobs(Long userId, String pageNumber, String pageSize) {
		Pageable pagination = Pegination.getPagination(pageNumber, pageSize);
		Page<IJobListDto> jobs = this.jobRepository.findByCreatedBy(userId, pagination, IJobListDto.class);
		return jobs;
	}

	@Override
	public Page<IJobListDto> getAllJobs(String pageNumber, String pageSize) {
		Pageable pagination = Pegination.getPagination(pageNumber, pageSize);
		Page<IJobListDto> jobs = this.jobRepository.findByOrderByIdDesc(pagination, IJobListDto.class);
		return jobs;
	}

	@Override
	public JobDto getJobApplicants(Long userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<IJobListDto> getJobsBySearch(String pageNumber, String pageSize, String search) {
		Pageable pagination = Pegination.getPagination(pageNumber, pageSize);
		Page<IJobListDto> jobs = this.jobRepository
				.findByTitleContainingIgnoreCaseOrDescriptionContainingIgnoreCaseOrderByIdDesc(search, search,
						pagination, IJobListDto.class);
		return jobs;
	}

//	@Override
//	public Page<RecruiterJobResponseDto> getAlljobsWithRecruiter(String pageNumber, String pageSize) {
//		Pageable pagination = Pegination.getPagination(pageNumber, pageSize);
//		Page<IUserJobsListDto> userJobs = this.jobRepository.getAllJobs(pagination, IUserJobsListDto.class);
////		Page<IUserJobsListDto> userJobs = this.userJobRepository.getAllUserJobs(pagination, IUserJobsListDto.class);
//		if (userJobs == null || userJobs.getContent().isEmpty()) {
//			throw new ResourceNotFoundException(ErrorKeyConstants.USER_JOB_E031701, "Not any job is listed");
//		}
//
//		Map<Long, RecruiterJobResponseDto> userJobResponseMap = new LinkedHashMap<>(); // Map to store jobUserResponse
//																						// by
//																						// jobId
//
//		for (IUserJobsListDto u : userJobs) {
//			JobDetails jobDetails = new JobDetails();
//			RecruiterDetails recruiterDetails = new RecruiterDetails();
//
//			jobDetails.setJobId(u.getJobId());
//			jobDetails.setJobTitle(u.getJobTitle());
//			recruiterDetails.setId(u.getId());
//			recruiterDetails.setName(u.getName());
//
//			if (userJobResponseMap.containsKey(u.getId())) {
//
//				RecruiterJobResponseDto userJobResponse = userJobResponseMap.get(u.getId());
//				userJobResponse.getJobDetails().add(jobDetails);
//			} else {
//
//				RecruiterJobResponseDto userJobResponse = new RecruiterJobResponseDto();
//				userJobResponse.setRecruiterDetails(recruiterDetails);
//				userJobResponse.setJobDetails(new ArrayList<>(Collections.singletonList(jobDetails)));
//				userJobResponseMap.put(u.getId(), userJobResponse);
//			}
//		}
//
//		List<RecruiterJobResponseDto> userJobResponseList = new ArrayList<>(userJobResponseMap.values());
//
//		return new PageImpl<>(userJobResponseList, pagination, userJobs.getTotalElements());
//	}

	@Override
	public Page<RecruiterJobResponseDto> getAlljobsWithRecruiter(String pageNumber, String pageSize) {
		Pageable pagination = Pegination.getPagination(pageNumber, pageSize);
		Page<IUserJobsListDto> userJobs = jobRepository.getAllJobs(pagination, IUserJobsListDto.class);

		if (userJobs == null || userJobs.isEmpty()) {
			throw new ResourceNotFoundException(ErrorKeyConstants.USER_JOB_E031701, "Not any job is listed");
		}

		List<RecruiterJobResponseDto> userJobResponseList = userJobs.stream()
				.collect(Collectors.groupingBy(IUserJobsListDto::getId)).entrySet().stream().map(entry -> {
					RecruiterDetails recruiterDetails = new RecruiterDetails(entry.getKey(),
							entry.getValue().get(0).getName());
					List<JobDetails> jobDetails = entry.getValue().stream()
							.map(dto -> new JobDetails(dto.getJobId(), dto.getJobTitle())).collect(Collectors.toList());
					return new RecruiterJobResponseDto(recruiterDetails, jobDetails);
				}).collect(Collectors.toList());

		return new PageImpl<>(userJobResponseList, pagination, userJobs.getTotalElements());
	}

	@Override
	public void DeleteJob(Long jobId, Long userId, List<String> permissions) {
		JobEntity job = this.jobRepository.findById(jobId)
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.JOB_E031401,
						ErrorMessageConstants.JOB_NOT_FOUND));
		if ((permissions.contains(Constants.IS_RECRUITER) && job.getCreatedBy().equals(userId))
				|| (permissions.contains(Constants.IS_ADMIN))) {
			this.jobRepository.delete(job);
		}
	}

}
