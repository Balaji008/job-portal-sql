package com.jobportalsql.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.jobportalsql.constants.CacheConstants;
import com.jobportalsql.constants.Constants;
import com.jobportalsql.dto.UserDto;
import com.jobportalsql.dto.UserResponseDto;
import com.jobportalsql.entities.RoleEntity;
import com.jobportalsql.entities.UserEntity;
import com.jobportalsql.entities.UserRoleEntity;
import com.jobportalsql.exception.ResourceNotFoundException;
import com.jobportalsql.ilistdto.IUserListDto;
import com.jobportalsql.ilistdto.IUserRoleListDto;
import com.jobportalsql.page.Pegination;
import com.jobportalsql.repository.RoleRepository;
import com.jobportalsql.repository.UserRepository;
import com.jobportalsql.repository.UserRoleRepository;
import com.jobportalsql.service.UserService;
import com.jobportalsql.util.CacheOperations;
import com.jobportalsql.util.ErrorKeyConstants;
import com.jobportalsql.util.ErrorMessageConstants;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository repository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserRoleRepository userRoleRepository;

	@Autowired
	private CacheOperations cacheOperations;

	@Override
	public UserResponseDto updateUser(Long id, UserDto userDto, Long adminId) {
		UserEntity entity = this.repository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.USER_E031101,
						ErrorMessageConstants.USER_NOT_FOUND));
		entity.setEmail(userDto.getEmail());
		entity.setName(userDto.getName());
		entity.setGender(userDto.getGender());
		entity.setPhoneNumber(userDto.getPhoneNumber());
		entity.setUpdatedBy(adminId);
		RoleEntity role = this.roleRepository.findById(userDto.getRoleId())
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.ROLE_E031201,
						ErrorMessageConstants.ROLE_NOT_FOUND));

		this.repository.save(entity);
		if (this.userRoleRepository.userExists(entity.getId())) {
			UserRoleEntity userRoleEntity = this.userRoleRepository.findByUserId(entity.getId());
			userRoleEntity.setRole(role);
			this.userRoleRepository.save(userRoleEntity);
		} else {
			UserRoleEntity userRoleEntity = new UserRoleEntity();
			userRoleEntity.setUser(entity);
			userRoleEntity.setRole(role);
			this.userRoleRepository.save(userRoleEntity);
		}

		String userId = entity.getId().toString();
		String key = CacheConstants.PERMISSIONS.concat(userId);
		cacheOperations.removeKeyFromCache(key);
		cacheOperations.removeKeyFromCache(entity.getEmail());
		UserResponseDto responseDto = new UserResponseDto();
		responseDto.setId(entity.getId());
		responseDto.setName(entity.getName());
		responseDto.setEmail(entity.getEmail());
		responseDto.setRoleName(role.getRoleName());
		responseDto.setGender(entity.getGender());
		responseDto.setPhonenumber(entity.getPhoneNumber());
		return responseDto;

	}

	@Override
	public void deleteUser(Long id) {
		UserEntity user = this.repository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.USER_E031101,
						ErrorMessageConstants.USER_NOT_FOUND));
		this.repository.delete(user);
	}

	@Override
	public Page<IUserListDto> getUserById(Long id, String pageNumber, String pageSize) {
		Pageable pagination = Pegination.getPagination(pageNumber, pageSize);
		Page<IUserListDto> users = this.repository.findById(id, pagination, IUserListDto.class);
		return users;
	}

//	@Override
//	public Page<IUserListDto> getUserByName(String name, String pageNumber, String pageSize) {
//		Pageable pagination = Pegination.getPagination(pageNumber, pageSize);
//		Page<IUserListDto> users = this.repository.findByNameContainingIgnoreCase(name, pagination, IUserListDto.class);
//		return users;
//	}

	@Override
	public Page<IUserRoleListDto> getAllUsersBySerach(String search, String pageNumber, String pageSize) {
		Pageable pagination = Pegination.getPagination(pageNumber, pageSize);
		Page<IUserRoleListDto> users = this.userRoleRepository.getAllUsersBySearch(search, pagination,
				IUserRoleListDto.class);
		return users;
	}

	@Override
	public Page<IUserRoleListDto> getAllUsers(String pageNumber, String pageSize) {
		RoleEntity role = this.roleRepository.findByRoleNameIgnoreCase(Constants.USER);
		Pageable pagination = Pegination.getPagination(pageNumber, pageSize);
		Page<IUserRoleListDto> users = this.userRoleRepository.getAllUsersByRole(role.getId(), pagination,
				IUserRoleListDto.class);

		return users;
	}

	@Override
	public Page<IUserRoleListDto> getAllRecruiters(String pageNumber, String pageSize) {
		RoleEntity role = this.roleRepository.findByRoleNameIgnoreCase(Constants.RECRUITER);
		Pageable pagination = Pegination.getPagination(pageNumber, pageSize);
		Page<IUserRoleListDto> users = this.userRoleRepository.getAllUsersByRole(role.getId(), pagination,
				IUserRoleListDto.class);

		return users;
	}

	@Override
	public Page<IUserRoleListDto> getAllAdmin(String pageNumber, String pageSize) {
		RoleEntity role = this.roleRepository.findByRoleNameIgnoreCase(Constants.ADMIN);
		Pageable pagination = Pegination.getPagination(pageNumber, pageSize);
		Page<IUserRoleListDto> users = this.userRoleRepository.getAllUsersByRole(role.getId(), pagination,
				IUserRoleListDto.class);

		return users;
	}

}
