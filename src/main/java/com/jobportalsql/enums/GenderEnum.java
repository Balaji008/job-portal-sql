package com.jobportalsql.enums;

public enum GenderEnum {

	MALE, FEMALE, OTHER;
}
